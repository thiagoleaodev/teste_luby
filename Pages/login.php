<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../public/style/bootstrap.min.css">
    <link rel="stylesheet" href="../public/style/login.css">
    <title>Document</title>
</head>
<body>
    <div class="card">
        <div class="title-card">
            <p class="title">Login Administrador</p>
        </div>
        <form method="POST" action="../php/valida.php">
            <input type="text" class="form-control" id="inputname" placeholder="Email" name="email">
            <input type="text" class="form-control" id="inputname" placeholder="Senha" name="senha">
            <button type="submit" class="btn">Entrar</button>
        </form>
            <?php if(isset($_SESSION['loginErro'])){
                echo '<div class="alert alert-danger" role="alert">';
                echo $_SESSION['loginErro'];
                unset($_SESSION['loginErro']);
                echo '</div>';
            }
            ?>
    </div>
</body>
</html>