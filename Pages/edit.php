<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../public/style/bootstrap.min.css">
    <link rel="stylesheet" href="../public/style/cadastro.css">
    <title>Document</title>
</head>
<body>
    <div class="card">
        <div class="title-card">
            <p class="title">Editar informações</p>
        </div>
        <form action="../Php/update.php" method="POST">
            <input value="<?php echo $nome ?>" type="text" class="form-control" id="inputname" placeholder="Nome Completo" name="nome">
            <input value="<?php echo $idade?> " type="text" class="form-control" id="inputname" placeholder="Idade" name="idade">
            <select class="form-control" id="exampleFormControlSelect1" placeholder="Sexo" name="sexo">
                <option selected><?php echo $sexo; ?></option>
                <option>Feminino</option>
                <option>Masculino</option>
                <option>Outro</option>
            </select>
            <select class="form-control" id="exampleFormControlSelect1" placeholder="Ano escolar" name="ano">
                <option selected><?php echo $ano; ?></option>
                <option>1º ano</option>
                <option>2º ano</option>
                <option>3º ano</option>
            </select>
            <button type="submit" class="btn" name="save">Confirmar</button>
        </form>
    </div>
</body>
</html>