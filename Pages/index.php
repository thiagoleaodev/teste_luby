<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../public/style/bootstrap.min.css">
    <link rel="stylesheet" href="../public/style/cadastro.css">
    <title>Document</title>
</head>
<body>
    <div class="card">
        <div class="title-card">
            <p class="title">Cadastro de alunos</p>
        </div>
        <form action="../php/include.php" method="POST">
            <input type="text" class="form-control" id="inputname" placeholder="Nome Completo" name="nome">
            <input type="text" class="form-control" id="inputname" placeholder="Idade" name="idade">
            <select class="form-control" id="exampleFormControlSelect1" placeholder="Sexo" name="sexo">
                <option value="" disabled selected>Sexo</option>
                <option>Feminino</option>
                <option>Masculino</option>
                <option>Outro</option>
            </select>
            <select class="form-control" id="exampleFormControlSelect1" placeholder="Ano escolar" name="ano">
                <option value="" disabled selected>Ano Escolar</option>
                <option>1º ano</option>
                <option>2º ano</option>
                <option>3º ano</option>
            </select>
            <button type="submit" class="btn" name="save">Cadastrar</button>
        </form>
    </div>
    <?php require_once '../Php/include.php'; ?>
</body>
</html>